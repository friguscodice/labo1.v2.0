#include "Fecha.h"
#include <iostream>

using namespace std;

// Constructor
Fecha::Fecha (int dia, int mes, int anio) {
	this->setDia (dia);
	this->setMes (mes);
	this->setAnio (anio);
}

// Destructor
Fecha::~Fecha (){
}

// Dia
int Fecha::getDia () const{
	return dd;
}
void Fecha::setDia (int dia){
	if (dia<1 || dia>31){
		cout << "Argumento invalido, dia debe estar entre 1 y 31" << endl;
	}
	dd = dia;
}

// Mes
int Fecha::getMes () const{
	return mm;
}
void Fecha::setMes (int mes){
	if (mes<1 || mes>12){
		cout << "Argumento invalido, mes debe estar entre 1 y 12" << endl;
	}
	mm = mes;
}

// Anio
int Fecha::getAnio () const{
	return aaaa;
}
void Fecha::setAnio (int anio){
	if (anio<1900){
		cout << "Argumento invalido, anio debe ser 1900 o mayor" << endl;
	}
	aaaa = anio;
}
