#ifndef CARAMELO_H
#define CARAMELO_H

#include "Golosina.h"

class Caramelo : public Golosina
{
	private:
		string sabor;
	public:
		// Constructor
		Caramelo (string _nombre, double _precio, const Fecha& FechaVencimiento, string _sabor, string tipo);
		// Destructor
		~Caramelo ();
		// 	Sabor
		string getSabor () const;
		void setSabor (string _sabor);
};

#endif /* CARAMELO_H */
