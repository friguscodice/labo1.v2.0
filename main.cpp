#include <iostream>
#include <string.h>
#include <stdio.h>
#include "Golosina.h"
#include "Caramelo.h"
#include "Alfajor.h"
#include "Fabricante.h"

#define Maxfabricantes 10

using namespace std;

Fabricante** fabricante;
int cantfabricante = 0;

void inicializo (Fabricante *fabricante[]);
void agregarFabricante (string _nombre, string _rut, string _pais);
string* listarFabricante (int &cantfabricante);
void agregargolosina (string _nombrefab, Golosina* golosina);
void eliminargolosina (string _nombrefab, string _nombregol);

int main(int argc, char** argv) {
	fabricante = new Fabricante*[Maxfabricantes];
	inicializo (fabricante);
	int opcion;
	do
	{
		//system ("CLS");
		cout << "-----------------------------------------------------" << endl;
		cout << "Seleccione una opcion y a continuacion presione ENTER" << endl;
		cout << "-----------------------------------------------------" << endl;
		cout << "1- Agregar Fabricante" << endl;
		cout << "2- Mostrar todos los fabricantes" << endl;
		cout << "3- Agregar una Golosina" << endl;
		cout << "4- Eliminar una Golosina" << endl;
		cout << "5- Obtener informacion de las Golosinas creadas por un Fabricante" << endl;
		cout << "6- Salir" << endl;
		cin >> opcion;
		//system ("CLS");
		switch (opcion){
			// Agregar Fabricante
			case 1:{
				string nombrefab, paisfab, rutfab;
				cout << "Ingrese nombre del fabricante" << endl;
				cin >> nombrefab;
				cout << "Ingrese pais del fabricante" << endl;
				cin >> paisfab;
				cout << "Ingrese RUT del fabricante" << endl;
				cin >> rutfab;
				agregarFabricante (nombrefab, rutfab, paisfab);
				//System ("pause");
				break;
			}
			// Lista de Fabricante
			case 2:{
				if (cantfabricante == 0)
					cout << "No hay fabricantes ingresados" << endl; 
				else{
					for (int c = 0; c < cantfabricante; c++){
						cout << "Fabricante " << c+1 << ": " << listarFabricante(cantfabricante)[c] << endl;
					}
				}
				//system ("pause");
				break;
			}
			// Agregar golosina
			case 3:{
				int otraopcion;
				do
				{
					//system ("CLS");
					cout << "-----------------------------------------------------" << endl;
					cout << "Seleccione una opcion y a continuacion presione ENTER" << endl;
					cout << "-----------------------------------------------------" << endl;
					cout << "1- Crear Caramelo" << endl;
					cout << "2- Crear Alfajor" << endl;
					cout << "3- Menu Anterior" << endl;
					cin >> otraopcion;
					//system ("CLS");
					switch (otraopcion){
						case 1:{
							string nombrefab, nombregol, sabor;
							int fechavendia, fechavenmes, fechavenanio;
							float precio;
							cout << "Ingrese nombre del Fabricante" << endl;
							cin >> nombrefab;
							cout << "Ingrese nombre del Caramelo" << endl;
							cin >> nombregol;
							cout << "Ingrese sabor del Caramelo" << endl;
							cin >> sabor;
							cout << "Ingrese precio del Caramelo" << endl;
							cin >> precio;
							cout << "Ingrese fecha de vencimiento del Caramelo" << endl;
							cin >> fechavendia >> fechavenmes >> fechavenanio;
							Fecha* fechaven = new Fecha(fechavendia, fechavenmes, fechavenanio);
							agregargolosina(nombrefab, new Caramelo(nombregol, precio, *fechaven, sabor, "Caramelo"));
							delete fechaven;		
							break;
						}
						case 2:{
							string nombrefab, nombregol;
							int fechavendia, fechavenmes, fechavenanio;
							float precio;
							cout << "Ingrese nombre del Fabricante" << endl;
							cin >> nombrefab;
							cout << "Ingrese nombre del Alfajor" << endl;
							cin >> nombregol;
							cout << "Ingrese precio del Alfajor" << endl;
							cin >> precio;
							cout << "Ingrese fecha de vencimiento del Alfajor" << endl;
							cin >> fechavendia >> fechavenmes >> fechavenanio;
							Fecha* fechaven = new Fecha(fechavendia, fechavenmes, fechavenanio);
							agregargolosina(nombrefab, new Alfajor(nombregol, precio, *fechaven, "Alfajor"));
							delete fechaven;						
							break;
						}
						case 3:{
							cout << "Regresando al menu anterior..." << endl; 
							break;
						}
						default:{
							cout << "Opcion invalida" << endl;
						}
					}
				}
				while (otraopcion != 1 && otraopcion != 2 && otraopcion != 3);
				//system ("pause");
				break;
			}
			// Eliminar Golosina
			case 4:{
				string nombrefab, nombregol;
				cout << "Ingrese nombre del Fabricante" << endl;
				cin >> nombrefab;
				cout << "Ingrese nombre de la Golosina a eliminar" << endl;
				cin >> nombregol;
				eliminargolosina (nombrefab, nombregol);
				//system ("pause");
                                getchar();
                                getchar();
				break;
			}
			// Obtener info de Golosina x Fabricante
			case 5:{
				cout << "Aun no implementado..." << endl;
				//system ("pause");
				break;
			}
			// Salir
			case 6:{
				cout << "Saliendo del programa..." << endl;
				//system ("pause");
				break;
			}
			default:{
				cout << "Opcion invalida" << endl;
				//system ("pause");
			}
		}
	}
	while (opcion != 6);
	return 0;
}

void agregarFabricante (string _nombre, string _rut, string _pais){
	bool y = true;
	int x = 0;
	if (cantfabricante == Maxfabricantes){
		cout << "Se alcanzo el numero maximo de fabricantes" << endl;
	}
	else{
		while (x < Maxfabricantes && y == true){
			if (fabricante[x]->getNombre() == _nombre){
				cout << _nombre << " ya fue creado" << endl;
				y = false;
				x = Maxfabricantes;
			}
			x++;
		}
		if (y == true){
			fabricante[cantfabricante]= new Fabricante(_nombre,_rut,_pais);
			cantfabricante ++;
			cout << "Fabricante creado!!!" << endl;
		}
	}
}

void inicializo (Fabricante *fabricante[]){
	for (int x = 0; x < Maxfabricantes; x++){
		fabricante[x]= new Fabricante ();
	}
}

string* listarFabricante (int &cantfabricante){
	if (cantfabricante == 0)
		return NULL;
	string* arrayfab = new string[cantfabricante];
    
    for(int x = 0 ; x < cantfabricante; x++){
        arrayfab[x] = fabricante[x]->getNombre();
    }
    return arrayfab;
}

void agregargolosina (string _nombrefab, Golosina* golosina){
	int pos = -1;
	for (int x = 0; x < cantfabricante; x++){
		if (fabricante[x]->getNombre() == _nombrefab){
			pos = x;
			x = cantfabricante;
		}
	}
	if (pos < 0){
		cout << "El fabricante " << _nombrefab << " no existe" << endl;
		delete golosina;
	}
	else{
		fabricante[pos]->agregarGolosina(golosina);
	}
}

void eliminargolosina (string _nombrefab, string _nombregol){
	int pos = -1;
	for (int x = 0; x < cantfabricante; x++){
		if (fabricante[x]->getNombre() == _nombrefab){
			pos = x;
			x = cantfabricante;
		}
	}
	if (pos < 0){
		cout << "El fabricante " << _nombrefab << " no existe" << endl;
	}
	else{
		fabricante[pos]->eliminarGolosina(_nombregol);
	}
}