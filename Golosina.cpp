#include "Golosina.h"
#include <iostream>

// Constructor
Golosina::Golosina (string _nombre, double _precio, const Fecha& FechaVencimiento, string tipo)
: fechavencimiento(FechaVencimiento){
	nombre = _nombre;
	precio = _precio;
	fabricante = NULL;
        this->tipo= tipo;
}

// Destructor
Golosina::~Golosina (){
}

// Nombre
string Golosina::getNombre () const{
	return nombre;
}
void Golosina::setNombre (string _nombre){
	nombre = _nombre;
}

// Precio
double Golosina::getPrecio () const{
	return precio;
}
void Golosina::setPrecio (double _precio){
	precio = _precio;
}

// Fecha vencimiento
Fecha Golosina::getFechavencimiento () const{
	return fechavencimiento;	
}
void Golosina::setFechavencimiento (const Fecha& FechaVencimiento){
	fechavencimiento = FechaVencimiento;
}

// Fabricante
Fabricante* Golosina::getFabricante() const{
	return fabricante;
}
void Golosina::setFabricante (Fabricante* Fabricante){
	fabricante = Fabricante;
}

std::ostream& operator << (std::ostream& retorno,const Golosina* golo)
{
	
	retorno << "Nombre : " << golo->nombre << endl;
	retorno << "Precio : " << golo->precio << endl;
	retorno << "Fabricante : " << golo->fabricante->getNombre() << endl;
	retorno << "Origen : " << golo->fabricante->getOrigen() << endl;
	
	return retorno;
}