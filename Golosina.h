#ifndef GOLOSINA_H
#define GOLOSINA_H

#include <string>
#include "Fecha.h"
#include "Fabricante.h"
using namespace std;


class Golosina
{
	private:
		string nombre;
		double precio;
		Fecha fechavencimiento;
		Fabricante *fabricante;
                string tipo;
	public:
		// Constructor
		Golosina (string _nombre, double _precio, const Fecha& fechavencimiento, string tipo);
		//Destructor
		~Golosina ();
		// Nombre
		string getNombre () const;
		void setNombre (string _nombre);
		// Precio
		double getPrecio () const;
		void setPrecio (double _precio);
		// Fecha Vencimiento
		Fecha getFechavencimiento () const;
		void setFechavencimiento (const Fecha& FechaVencimiento);
		// Fabricante
		Fabricante* getFabricante () const;
		void setFabricante (Fabricante*);
		//Sobrecarga
		friend std::ostream& operator << (std::ostream& retorno,const Golosina* golo);
};

#endif /* GOLOSINA_H */
