#ifndef FECHA_H
#define FECHA_H

class Fecha{
	private:
		int dd;
		int mm;
		int aaaa;
	public:
		// Constructor
		Fecha (int dia, int mes, int anio);
		// Destructor
		~Fecha ();
		// Dia
		int getDia () const;
		void setDia (int dia);
		// Mes
		int getMes () const;
		void setMes (int mes);
		// Anio
		int getAnio () const;
		void setAnio (int anio);
};

#endif /* FECHA_H */
