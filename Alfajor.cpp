#include "Alfajor.h"

// Constructor
Alfajor::Alfajor (string _nombre, double _precio, const Fecha& FechaVencimiento,string _nombreFab,string _Origenfab,string _tipo)
:Golosina (_nombre, _precio, FechaVencimiento,_nombreFab,_Origenfab,_tipo){
}

// Destructor
Alfajor::~Alfajor (){
}
//SobreCarga
std::ostream& operator<<(std::ostream& retorno, const Alfajor* alfa)
{
	retorno << "Nombre : " << alfa->getNombre();
	retorno << "Precio : $" << alfa->getPrecio();
	retorno << "Fabricante : " << alfa->getNombrefab();
	retorno <<  "Origen : " << alfa->getOrigenfab();
	retorno << "Tipo : " << alfa->getTipo();
}