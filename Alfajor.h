#ifndef ALFAJOR_H
#define ALFAJOR_H

#include "Golosina.h"

class Alfajor : public Golosina
{
	public:
		// Constructor
		Alfajor (string _nombre, double _precio, const Fecha& FechaVencimiento, string tipo);
		// Destructor
		~Alfajor ();
};

#endif /* ALFAJOR_H */
