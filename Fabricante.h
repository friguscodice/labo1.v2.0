#ifndef FABRICANTE_H
#define	FABRICANTE_H

#include <iostream>
#include <string.h>
#include <stdio.h>
#include "Enumorigen.h"
#define Maxgolosinas 50
class Golosina;
using namespace std;

class Fabricante{
	private:
		string nombre;
		string rut;
		Origen origen;
		string pais;
		Golosina** golosinas;
		int cantgolosinas;
	public:
		// Constructor
		Fabricante (string _nombre = "0", string _rut = "0", string _pais = "0");
		// Destructor
		~Fabricante ();
		// Nombre
		string getNombre () const;
		void setNombre (string _Nombre);
		// Rut
		string getRut () const;
		void setRut (string _rut);
		// Origen
		Origen getOrigen () const;
		// Pais
		string getPais () const;
		void setPais (string _pais);
		
		// Comportamiento
		void agregarGolosina(Golosina* golosina);
		void eliminarGolosina(string _nombregol);
};


#endif	/* FABRICANTE_H */