#include "Caramelo.h"

// Constructor
Caramelo::Caramelo (string _nombre, double _precio, const Fecha& FechaVencimiento, string _sabor, string tipo)
:Golosina (_nombre, _precio, FechaVencimiento, tipo){
	sabor = _sabor;
}

// Destructor
Caramelo::~Caramelo (){
}

//Sabor
string Caramelo::getSabor () const{
	return sabor;
}
void Caramelo::setSabor (string _sabor){
	sabor = _sabor;
}
