#include "Fabricante.h"
#include "Golosina.h"
#include <iostream>

// Constructor
Fabricante::Fabricante(string _nombre, string _rut, string _pais){
	nombre = _nombre;
	rut = _rut;
	this->setPais(_pais);
	this->golosinas = new Golosina*[Maxgolosinas];
	cantgolosinas = 0;
	int x = 0;
	while (x < Maxgolosinas){
		golosinas[x]= new Golosina("-",0,Fecha(1,1,1900),"-");
		x++;
	}
}

// Destructor
Fabricante::~Fabricante(){
};

// Nombre
string Fabricante::getNombre() const{
	return nombre;
}
void Fabricante::setNombre (string _nombre){
	nombre = _nombre;
}

// Rut
string Fabricante::getRut() const{
	return rut;
}
void Fabricante::setRut (string _rut){
	rut = _rut;
}

// Pais
string Fabricante::getPais() const{
	return pais;
}
void Fabricante::setPais (string _pais){
	pais = _pais;
	if (_pais == "Uruguay" || _pais == "uruguay")
		origen = Nacional;
	else
		origen = Importado;
}

// Origen
Origen Fabricante::getOrigen () const{
	return origen;
}

// Comportamiento
void Fabricante::agregarGolosina (Golosina* golosina){
	int y = 0;
	if (cantgolosinas == Maxgolosinas){
		delete golosina;
		cout << "Limite de golosinas alcanzado" << endl;
		y = 1;
	}
	for (int x = 0; x < Maxgolosinas; x++){
		if (this->golosinas[x]->getNombre() == golosina->getNombre()){
			delete golosina;
			cout << "Este fabricante ya creo esta golosina" << endl;
			y = 1;
		}
	}
	if (y == 0){
		golosina->setFabricante(this);
		this->golosinas[cantgolosinas] = golosina;
  	  	cantgolosinas++;
  	  	cout << "Golosina creada!!!" << endl;
	}	
}

void Fabricante::eliminarGolosina(string _nombregol){
	int pos = -1;
	for (int x = 0; x < cantgolosinas; x++){
		if(this->golosinas[x]->getNombre() == _nombregol){
			pos = x;
			x = cantgolosinas;
		}
	}
	if (pos < 0){
		cout << "No existe la golosina " << _nombregol << endl;
	}
	else{
		while(pos < cantgolosinas){
			golosinas[pos] = golosinas[pos+1];
			pos ++;
		}
		cantgolosinas--;
		cout << "Golosina elminada!!!" << endl;
	}
}